import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np
from memory import ReadHead, WriteHead
from feedforward import FeedFwdController
from lstm import LSTM

class NTM(nn.Module):
    def __init__(self, n_inputs, sz_controller, n_outputs, N, M, model):
        """
        :n_inputs = size of input  (len(x) + M) because 1 read head
        :sz_controller = controller size (100)
        :n_outputs = output size (same is input for copy task)
        :N = number of rows in memory
        :M = number of columns in memory
        :model = either LSTM or feedforward
        """
        super(NTM, self).__init__()
        self.N = N
        self.M = M
        self.controller = model(n_inputs + M, sz_controller, n_outputs)
        self.w_head = WriteHead(N, M, sz_controller)
        self.r_head = ReadHead(N, M, sz_controller)
        self.dense = nn.Linear(sz_controller, n_outputs)
        self.token = Variable(torch.zeros(1,n_inputs))
        self.stdev = 1 / (np.sqrt(N + M))

    # Pass twice over mini-batch then predict, memory is reinitialized for each mini-batch
    def forward(self, x):
        x = x.squeeze(1)
        print(x)
        self.controller.hidden = self.controller.create_new_state(1) # batch_size always 1
        
        # initialize memory, read/write weights and prediction matrix
        M_t = Variable(nn.init.uniform(torch.randn(self.N, self.M), -self.stdev, self.stdev)) # read/write memory
        ww_t = F.softmax(Variable(torch.arange(self.N, 0, -1)), dim=0)
        rw_t = F.softmax(Variable(torch.arange(self.N, 0, -1)), dim=0)
        r_t  = F.tanh(Variable(torch.randn(1,self.M)))
        seq_out = torch.autograd.Variable(torch.zeros(x.data.shape)) # predictions

        # start token
        inp = torch.cat([self.token, r_t], dim=1)
        controller_out, controller_state = self.controller(inp.unsqueeze(1), self.controller.hidden)
        M, ww, rw, r_t = self._read_write(M_t, rw_t, ww_t, controller_out)

        # feed sequence + delimiter
        for t in range(len(x)):
            inp = torch.cat([x[t], r_t], dim=1)
            controller_out, controller_state = self.controller(inp.unsqueeze(1), controller_state)
            M, ww, rw, r_t = self._read_write(M_t, rw_t, ww_t, controller_out)

        # end token
        inp = torch.cat([self.token, r_t], dim=1)
        controller_out = self.controller(inp.unsqueeze(1), controller_state)
        M, ww, rw, r_t = self._read_write(M_t, rw_t, ww_t, controller_out)

        for t in range(len(x)):
            inp = torch.cat([x[t], r_t], dim=1)
            controller_out, controller_state = self.controller(inp.unsqueeze(1), controller_state)
            M, ww, rw, r_t = self._read_write(M_t, rw_t, ww_t, controller_out)
            seq_out[t] = F.sigmoid(self.dense(r_t))
        
        # returns predictions
        return seq_out
    
    # write to memory, then read
    def _read_write(self, M_t, rw_t, ww_t, controller_out):
        M_t, ww_t = self.w_head.forward(M_t, ww_t, controller_out)
        r_t, rw_t = self.r_head.forward(M_t, rw_t, controller_out)
        return M_t, ww_t, rw_t, r_t

    def size(self):
        size = 0
        for p in self.parameters():
            size += p.data.view(-1).size(0)
        return size


def dataloader(n_batches, bs, seq_width, min_len, max_len):
    for b in range(n_batches):
        # determine seq length for this mini-batch
        seq_len = np.random.randint(min_len, max_len)
        # generate sequence
        seq = np.random.binomial(1, 0.5, (seq_len, bs, seq_width))
        # to torch Variable
        seq = Variable(torch.from_numpy(seq))
        # generate input to be
        inp = Variable(torch.zeros(seq_len + 1, bs, seq_width + 1))
        # fill in with seq
        inp[:seq_len, :, :seq_width] = seq
        # last line has 1 for end
        inp[seq_len, :, seq_width] = 1
        # outp is label
        outp = seq.clone()
        yield inp.float(), outp.float()

def train_lstm(model, epochs, trainloader,optim,criterion, n_batches):
    train_losses = []
    for epoch in range(epochs):
        running_loss = 0
        for seq,exp in trainloader:
            model.zero_grad()
            out = model(seq)
            loss = criterion(out[:-1],exp) # flush last line
            loss.backward()
            optim.step()
            running_loss += loss.data[0]
            i += 1
        mean_loss = running_loss/n_batches
        print(mean_loss)
        train_losses.append(mean_loss)
    return train_losses



###
### TESTING lstm
###
n_inputs = 9
N = 128 
M = 20
n_outputs = 8
sz_controller = 100
epochs = 20
criterion = nn.BCELoss()
lr = 0.01
bs = 1
n_batches = 1000
trainloader = list(dataloader(n_batches,bs,8,1,20))

#Merci d'avoir voyagé avec la LSTM
mccarold = NTM(n_inputs,sz_controller,n_outputs, N, M, LSTM)
#hawkins = NTM(n_inputs,n_outputs,N,M,LSTM)

optim = torch.optim.SGD(mccarold.parameters(),lr=lr)

losses = train_lstm(mccarold,epochs,trainloader,optim,criterion, n_batches)

import torch
import torch.nn as nn
from torch.autograd import Variable

# Feedforward controller
# one hidden layer of size 100
class FeedFwdController(nn.Module):
    def __init__(self, n_inputs, sz_controller, n_outputs):
        """
        :n_inputs = len(x) + M (bcuz 1 read head)
        :sz_controller = 100 (fixed)
        :n_outputs = 8 (size of prediction, or M)
        """
        super().__init__()
        self.h1 = torch.nn.Linear(n_inputs, sz_controller)
        self.hidden = 0
    
    # does nothing just for compatibility with LSTM controller
    def create_new_state(self, batch_size):
        pass
    
    # poubelle just placeholder for compatibility with LSTM controller
    # returns output of sz_controller
    def forward(self, x, poubelle):
        x = torch.nn.functional.sigmoid(self.h1(x))
        return out, poubelle

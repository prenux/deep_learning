import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.autograd import Variable

CONV_SHIFT = 3

class Head(nn.Module):
    def __init__(self, N, M, sz_controller):
        super().__init__()
        self.sz_params = [N, 1, 1, CONV_SHIFT, 1] # TODO M or N
        self.layers = nn.ModuleList([(nn.Linear(sz_controller, i)) for i in self.sz_params])

    def address(self, mem, k, beta, w_prev, g, s, gamma):
        # Regularizing the parameters
        beta = F.softplus(beta)         # beta > 0
        gamma = 1 + F.softplus(gamma)   # gamma > 1
        g = F.sigmoid(g)                # g \in (0, 1)
        s = F.softmax(s, dim=1)         # s is normalized 
        # Focusing by content
        normalized_w = self._similarity(k, beta, mem)
        # Focusing by location
        gated_w = g * normalized_w + (1 - g) * w_prev 
        shift_w = self._shift(gated_w, s)
        w = self._sharpen(shift_w, gamma)
        return w

    def _convolve(self, gated_w, s):
        t = torch.cat([gated_w[-1:], gated_w, gated_w[:1]])
        c = F.conv1d(t.view(1, 1, -1), s.view(1, 1, -1)).view(-1)
        return c

    def _similarity(self, k, beta, mem):
        return F.softmax(beta * F.cosine_similarity(k, mem), dim=1)

    def _shift(self, gated_w, s):
        result = Variable(torch.zeros(gated_w.size()))
        for b in range(self.batch_size):
            result[b] = self._convolve(gated_w[b], s[b])
        return result

    def _sharpen(self, shift_w, gamma):
        return (shift_w ** gamma) / torch.sum(shift_w ** gamma)


class ReadHead(Head):
    def __init__(self, N, M, sz_controller):
        super().__init__(N, M, sz_controller)

    def forward(self, mem, w_t, controller_output):
        k, beta, g, s, gamma = [layer(controller_output) for layer in self.layers]
        w = self.address(mem, k, beta, w_prev, g, s, gamma)
        return w.matmul(mem), w


class WriteHead(Head):
    def __init__(self, N, M, sz_controller):
        super().__init__(N, M, sz_controller)
        self.layers.extend([nn.Linear(sz_controller, M)] * 2)

    def forward(self, mem, w_prev, controller_output):
        k, beta, g, s, gamma, e, a = [layer(controller_output) for layer in self.layers]
        e = F.sigmoid(e)
        w = self.address(mem, k, beta, w_prev, g, s, gamma)
        mem_tilde = mem * (1 - w.unsqueeze(1).matmul(e.unsqueeze(0)))
        return mem_tilde + w.unsqueeze(1).matmul(a.unsqueeze(0)), w

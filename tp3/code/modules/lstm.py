import torch
import torch.nn as nn
from torch.nn import Parameter
from torch.autograd import Variable

# Implementation LSTM controller
class LSTM(nn.Module):
    def __init__(self, n_inputs,sz_controller,n_outputs,n_layers=1):
        super(LSTM, self).__init__()
        self.n_inputs = n_inputs
        self.sz_controller = sz_controller
        self.n_outputs = n_outputs
        self.n_layers = n_layers
        # LSTM
        self.lstm = nn.LSTM(self.n_inputs,
                            self.sz_controller,
                            self.n_layers)
        self.dense = nn.Linear(self.sz_controller,n_outputs)
        # hidden state
        self.hidden = (Variable(torch.nn.init.xavier_uniform(torch.randn(self.n_layers, 1, self.sz_controller))),
                Variable(torch.nn.init.xavier_uniform(torch.randn(self.n_layers, 1, self.sz_controller))))
        # init params
        self.init_params()

    def create_new_state(self, batch_size):
#        # hidden state
#        self.hidden = (Variable(torch.nn.init.xavier_uniform(torch.randn(self.n_layers, 1, self.n_outputs))),
#                Variable(torch.nn.init.xavier_uniform(torch.randn(self.n_layers, 1, self.n_outputs))))
#        # Dimension: (num_layers * num_directions, batch, hidden_size)
#        lstm_h0 = self.hidden[0].clone().repeat(1, batch_size, 1)
#        lstm_h1 = self.hidden[1].clone().repeat(1, batch_size, 1)
        hidden = (Variable(torch.randn(1, batch_size, self.sz_controller)), Variable(torch.randn((1, batch_size, self.sz_controller))))
        return hidden

    # Init all parameters
    def init_params(self):
        for p in self.lstm.parameters():
            if p.dim() == 1:
                nn.init.constant(p, 0)
            else:
                nn.init.xavier_uniform(p)

    def forward(self, x, state):
        out,state = self.lstm(x,state)
        return out,state


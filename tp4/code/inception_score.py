import torch
from torch import nn
from torch.autograd import Variable
from torch.nn import functional as F
import torch.utils.data

from torchvision.models.inception import inception_v3
import torchvision.datasets as dsets
import torchvision.transforms as transforms

import numpy as np
from scipy.stats import entropy


def inception_score(path_to_generated, batch_size=5, resize=True, splits=1):
    """Computes the inception score of the generated images imgs
    batch_size -- batch size for feeding into Inception v3
    splits -- number of splits
    """
    # Set up dtype
    dtype = torch.FloatTensor

    # Dataloader with images (64 x 64)
    dataset = dsets.ImageFolder(root=path_to_generated, transform=transforms.ToTensor())
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=True,num_workers=2)
    N = len(dataset)

    # Load inception model
    inception_model = inception_v3(pretrained=True, transform_input=False).type(dtype)
    inception_model.eval();
    up = nn.Upsample(size=(299, 299), mode='bilinear').type(dtype)
    def get_pred(data):
        if resize:
            data = up(data)
        data = inception_model(data)
        return F.softmax(data, dim=1).data.cpu().numpy()

    # Get predictions
    preds = np.zeros((N, 1000))

    for i, batch in enumerate(dataloader, 0):
        data, labels = batch
        data, labels = Variable(data), Variable(labels)
        batch_size_i = data.size()[0]
        preds[i*batch_size:i*batch_size + batch_size_i] = get_pred(data)

    # Now compute the mean kl-div
    split_scores = []

    for k in range(splits):
        part = preds[k * (N // splits): (k+1) * (N // splits), :]
        py = np.mean(part, axis=0)
        scores = []
        for i in range(part.shape[0]):
            pyx = part[i, :]
            scores.append(entropy(pyx, py))
        split_scores.append(np.exp(np.mean(scores)))

    return np.mean(split_scores), np.std(split_scores)

if __name__ == '__main__':
    print ("Calculating Inception Score...")
    mean, std = inception_score(splits=10)
    print("Mean of scores: {:.4f}, Std of scores: {:.4f}".format(mean,std))

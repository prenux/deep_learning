import os
import matplotlib.pyplot as plt
from scipy.misc import imresize

# number to preprocess
n = 1000

# root path depends on your computer
root = '../data/img_align_celeba/'
save_root = '../data/resized_celebA/'
resize_size = 64

if not os.path.isdir(save_root):
    os.mkdir(save_root)
if not os.path.isdir(os.path.join(save_root, 'real')):
    os.mkdir(os.path.join(save_root, 'real'))
img_list = os.listdir(root)

# ten_percent = len(img_list) // 10

for i in range(n):
    img = plt.imread(root + img_list[i])
    img = imresize(img, (resize_size, resize_size))
    plt.imsave(fname=os.path.join(save_root, 'real', img_list[i]), arr=img)

    if (i % 1000) == 0:
        print('%d images complete' % i)

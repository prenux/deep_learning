from simple_gan import *
import torchvision.datasets as dsets
import torchvision.transforms as transforms
import torch.optim as optim
#to show imgs
from PIL import Image
from torchvision.transforms import ToPILImage
import numpy as np

to_img = ToPILImage()

latent_sz = 100
img_sz = 64
bs = 128
n_img = 500

def load_gen(path,net):
    input_sampler = lambda bs, latent_sz: torch.FloatTensor(bs, latent_sz, 1, 1).normal_(0,1)
    moriarty = Generator(latent_sz,img_sz,input_sampler,net)
    moriarty.load_state_dict(torch.load(path))
    moriarty.eval()
    return moriarty

def save_output(moriarty,n_img,model_dir):
    moriarty.eval()
    imgs = []
    for i in range(n_img):
        img = to_img(moriarty(Variable(moriarty.input_sampler(1,moriarty.latent_sz))).data.squeeze(0))
        img.save(os.path.join("generated",model_dir,"gen-img_{}.jpg".format(i)))
        imgs.append(img)
    moriarty.train()


# ## Deconvolution

deconv= nn.Sequential(
    nn.ConvTranspose2d(latent_sz, img_sz * 8, 4, 1, 0, bias=False),
    nn.BatchNorm2d(img_sz * 8),
    nn.ReLU(True),
    # state size. (img_sz*8) x 4 x 4
    nn.ConvTranspose2d(img_sz * 8, img_sz * 4, 4, 2, 1, bias=False),
    nn.BatchNorm2d(img_sz * 4),
    nn.ReLU(True),
    # state size. (img_sz*4) x 8 x 8
    nn.ConvTranspose2d(img_sz * 4, img_sz * 2, 4, 2, 1, bias=False),
    nn.BatchNorm2d(img_sz * 2),
    nn.ReLU(True),
    # state size. (img_sz*2) x 16 x 16
    nn.ConvTranspose2d(img_sz * 2, img_sz, 4, 2, 1, bias=False),
    nn.BatchNorm2d(img_sz),
    nn.ReLU(True),
    # state size. (img_sz) x 32 x 32
    nn.ConvTranspose2d(img_sz, 3, 4, 2, 1, bias=False),
    nn.Tanh()
    # state size. (nc) x 64 x 64
    )

# Generator
moriarty0 = load_gen("models/celebA/dcgan/deconv/moriarty_100.sav",deconv)


# ## Nearest-Neighbor Upsampling

nn_upsampling = nn.Sequential(
    # 1x1
    nn.Upsample(scale_factor=2,mode='nearest'),
    nn.Conv2d(latent_sz,img_sz*8,2,stride=2,padding=1),
    nn.BatchNorm2d(img_sz * 8),
    nn.ReLU(True),
    # 2x2
    nn.Upsample(scale_factor=2,mode='nearest'),
    nn.Conv2d(img_sz*8,img_sz*4,2,stride=2,padding=2),
    nn.BatchNorm2d(img_sz * 4),
    nn.ReLU(True),
    # 4x4
    nn.Upsample(scale_factor=2,mode='nearest'),
    nn.Conv2d(img_sz*4,img_sz*2,5,stride=1,padding=2),
    nn.BatchNorm2d(img_sz * 2),
    nn.ReLU(True),
    # 8x8
    nn.Upsample(scale_factor=2,mode='nearest'),
    nn.Conv2d(img_sz*2,img_sz,5,stride=1,padding=2),
    nn.BatchNorm2d(img_sz),
    nn.ReLU(True),
    # 16x16
    nn.Upsample(scale_factor=2,mode='nearest'),
    nn.Conv2d(img_sz,img_sz,5,stride=1,padding=2),
    nn.BatchNorm2d(img_sz),
    nn.ReLU(True),
    # 32x32
    nn.Upsample(scale_factor=2,mode='nearest'),
    nn.Conv2d(img_sz,3,2,stride=1,padding=1),
    nn.Tanh()
    # 64x64
    )

# Generator
moriarty1 = load_gen("models/celebA/dcgan/nn_upsampling/moriarty_100.sav",nn_upsampling)

# ## Bilinear Upsampling

bilinear_upsampling = nn.Sequential(
    # 1x1
    nn.Upsample(scale_factor=2,mode='bilinear'),
    nn.Conv2d(latent_sz,img_sz*8,2,stride=2,padding=1),
    nn.BatchNorm2d(img_sz * 8),
    nn.ReLU(True),
    # 2x2
    nn.Upsample(scale_factor=2,mode='bilinear'),
    nn.Conv2d(img_sz*8,img_sz*4,2,stride=2,padding=2),
    nn.BatchNorm2d(img_sz * 4),
    nn.ReLU(True),
    # 4x4
    nn.Upsample(scale_factor=2,mode='bilinear'),
    nn.Conv2d(img_sz*4,img_sz*2,5,stride=1,padding=2),
    nn.BatchNorm2d(img_sz * 2),
    nn.ReLU(True),
    # 8x8
    nn.Upsample(scale_factor=2,mode='bilinear'),
    nn.Conv2d(img_sz*2,img_sz,5,stride=1,padding=2),
    nn.BatchNorm2d(img_sz),
    nn.ReLU(True),
    # 16x16
    nn.Upsample(scale_factor=2,mode='bilinear'),
    nn.Conv2d(img_sz,img_sz,5,stride=1,padding=2),
    nn.BatchNorm2d(img_sz),
    nn.ReLU(True),
    # 32x32
    nn.Upsample(scale_factor=2,mode='bilinear'),
    nn.Conv2d(img_sz,3,2,stride=1,padding=1),
    nn.Tanh()
    # 64x64
    )

# Generator
moriarty2 = load_gen("models/celebA/dcgan/bilinear_upsampling/moriarty_100.sav",bilinear_upsampling)

# # WGAN

# Generator
moriarty3 = load_gen("models/celebA/wgan/deconv/moriarty_100.sav",deconv)

save_output(moriarty0,n_img,'celebB/dcgan/deconv')
save_output(moriarty1,n_img,'celebB/dcgan/nn_upsampling')
save_output(moriarty2,n_img,'celebB/dcgan/bilinear_upsampling')
save_output(moriarty3,n_img,'celebB/wgan/deconv')

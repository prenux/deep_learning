import os
import torch
import torchvision.datasets as dsets
import torchvision.transforms as transforms
from torch.autograd import Variable
from simple_gan import Discriminator

# Define what we need
PATH_TO_VALID_SET = '../data/resized_celebA/'

def wasserstein_distance(path_to_discriminator, path_to_generated, n_samples=500, batch_size=5):
    """
    n_samples := number of sample scores to average
    """
    # Load the pre-trained WGAN model
    D = Discriminator(64, apply_sigmoid=False)
    D.load_state_dict(torch.load(path_to_discriminator))

    # Compute score for real and fake imgs
    real_score = wasserstein_score(D, PATH_TO_VALID_SET, batch_size, n_samples)
    fake_score = wasserstein_score(D, path_to_generated, batch_size, n_samples)

    return real_score - fake_score


def wasserstein_score(D, path_to_images, batch_size, n_samples):
    """
    Same as above except path_to_images can be either valid set or generated images
    """
    # Set up dtype
    dtype = torch.FloatTensor

    # Dataloader with images (64 x 64), adjust for n_samples
    dataset = dsets.ImageFolder(root=path_to_images, transform=transforms.ToTensor())
    dataset.imgs = dataset.imgs[:n_samples]
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=True,num_workers=2)

    # Get scores
    scores = torch.zeros(0)
    for i, batch in enumerate(dataloader, 0):
        data, labels = batch
        data, labels = Variable(data), Variable(labels)
        batch_size_i = data.size()[0]
        # TODO need to check output from D to make sure this works
        scores = torch.cat([scores, D(data).data])

    # Return mean of scores
    return torch.mean(scores)


if __name__ == '__main__':
    print ("Computing Wasserstein distance...")
    dist = wasserstein_distance()
    print("Wasserstein distance is: {:.8f}".format(dist))

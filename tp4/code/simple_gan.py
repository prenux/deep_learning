import os
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

###
### Generator
###
class Generator(nn.Module):
    def __init__(self, latent_sz, output_sz,input_sampler,net):
        super(Generator, self).__init__()
        self.latent_sz = latent_sz
        self.input_sampler = input_sampler
        self.net = net

    def set_optimizer(self, optimizer):
        self.optimizer = optimizer

    def forward(self, noise):
        return self.net(noise)

###
### Discriminator
###
class Discriminator(nn.Module):
    def __init__(self,img_input_sz,apply_sigmoid=True):
        super(Discriminator, self).__init__()
        layers = [
                # input is 3 x 64 x 64
                nn.Conv2d(3, img_input_sz, 4, 2, 1, bias=False),
                nn.LeakyReLU(0.2, inplace=True),
                # state size. (img_input_sz) x 32 x 32
                nn.Conv2d(img_input_sz, img_input_sz * 2, 4, 2, 1, bias=False),
                nn.BatchNorm2d(img_input_sz * 2),
                nn.LeakyReLU(0.2, inplace=True),
                # state size. (img_input_sz*2) x 16 x 16
                nn.Conv2d(img_input_sz * 2, img_input_sz * 4, 4, 2, 1, bias=False),
                nn.BatchNorm2d(img_input_sz * 4),
                nn.LeakyReLU(0.2, inplace=True),
                # state size. (img_input_sz*4) x 8 x 8
                nn.Conv2d(img_input_sz * 4, img_input_sz * 8, 4, 2, 1, bias=False),
                nn.BatchNorm2d(img_input_sz * 8),
                nn.LeakyReLU(0.2, inplace=True),
                # state size. (img_input_sz*8) x 4 x 4
                nn.Conv2d(img_input_sz * 8, 1, 4, 1, 0, bias=False)
                ]
        if apply_sigmoid:
            layers.append(nn.Sigmoid())
        self.main = nn.Sequential(*layers)

    def set_optimizer(self, optimizer):
        self.optimizer = optimizer

    def forward(self, data):
        return self.main(data).view(-1,1).squeeze(1)


def train_gan(epochs,sherlock,moriarty,k_steps,trainloader,criterion,model_dir=None,show_func=None):
    """
    :param epochs: number of epochs
    :param sherlock: discriminator
    :param moriarty: generator
    :param k_steps: num of steps to train moriarty per mini_batch
    :param trainloader: training dataloader
    :param criterion: criterion
    """
    for epoch in range(epochs):  # loop over the dataset multiple times
        print("Epoch",epoch)
        for i,real_data in enumerate(trainloader,0):
            # train sherlock on real images
            # zero the parameter gradients
            sherlock.optimizer.zero_grad()
            # get the inputs
            real_inputs, _ = real_data
            bs = real_inputs.size(0)
            real_inputs = Variable(real_inputs)
            # forward + backward + optimize
            # ones = real
            preds_real = sherlock(real_inputs)
            preds_real_loss = criterion(preds_real, Variable(torch.ones(bs)))

            # train sherlock on fake images
            # generate fake images (dont detach cuz zero_grad after)
            noise = moriarty.input_sampler(bs,moriarty.latent_sz)
            fake_inputs = moriarty(Variable(noise))
            # get sherlock's opinion
            preds_fake = sherlock(fake_inputs)
            # zeros = fake
            preds_fake_loss = criterion(preds_fake, Variable(torch.zeros(bs)))

            # calc tot loss + backward
            sher_loss = preds_real_loss + preds_fake_loss
            sher_loss.backward()
            sherlock.optimizer.step()

            # train moriarty
            for j in range(k_steps):
                moriarty.optimizer.zero_grad()
                # gen images
                fake_inputs = moriarty(Variable(moriarty.input_sampler(bs,moriarty.latent_sz)))
                # Not detaching since zero_grad next iter
                sherlocks_opinion = sherlock(fake_inputs)
                # Pretend it's real
                gen_loss = criterion(sherlocks_opinion, Variable(torch.ones(bs)))
                gen_loss.backward()
                moriarty.optimizer.step()

        if model_dir is not None:
            if show_func is not None:
                show_func(moriarty,5,epoch,model_dir=model_dir)
            if (epoch+1) % (epochs//10) == 0:
                torch.save(sherlock.state_dict(), os.path.join("models",model_dir,"sherlock_{}.sav".format(epoch+1)))
                torch.save(moriarty.state_dict(), os.path.join("models",model_dir,"moriarty_{}.sav".format(epoch+1)))

    if model_dir is not None:
        if show_func is not None:
            show_func(moriarty,10,epoch,model_dir=model_dir)
        torch.save(sherlock.state_dict(), os.path.join("models",model_dir,"sherlock_{}.sav".format(epochs)))
        torch.save(moriarty.state_dict(), os.path.join("models",model_dir,"moriarty_{}.sav".format(epochs)))
    print('Finished Training')

def train_wgan(epochs,sherlock,moriarty,k_steps,trainloader,criterion,model_dir=None,show_func=None):
    """
    :param epochs: number of epochs
    :param sherlock: discriminator
    :param moriarty: generator
    :param k_steps: num of steps to train moriarty per mini_batch
    :param trainloader: training dataloader
    :param criterion: criterion
    """
    for epoch in range(epochs):  # loop over the dataset multiple times
        print("Epoch",epoch)
        for i,real_data in enumerate(trainloader,0):
            # train sherlock on real images
            # zero the parameter gradients
            sherlock.optimizer.zero_grad()
            # get the inputs
            real_inputs, _ = real_data
            bs = real_inputs.size(0)
            real_inputs = Variable(real_inputs)
            # forward + backward + optimize
            # ones = real
            preds_real = sherlock(real_inputs)
            preds_real_loss = -torch.mean(preds_real)

            # train sherlock on fake images
            # generate fake images (dont detach cuz zero_grad after)
            noise = moriarty.input_sampler(bs,moriarty.latent_sz)
            fake_inputs = moriarty(Variable(noise))
            # get sherlock's opinion
            preds_fake = sherlock(fake_inputs)
            # zeros = fake
            preds_fake_loss = torch.mean(preds_fake)

            # calc tot loss + backward
            sher_loss = preds_real_loss + preds_fake_loss
            sher_loss.backward()
            sherlock.optimizer.step()

            ## Weight clipping
            for p in sherlock.parameters():
                p.data.clamp_(-0.05, 0.05)

            # train moriarty
            for j in range(k_steps):
                moriarty.optimizer.zero_grad()
                # gen images
                fake_inputs = moriarty(Variable(moriarty.input_sampler(bs,moriarty.latent_sz)))
                # Not detaching since zero_grad next iter
                sherlocks_opinion = sherlock(fake_inputs)
                gen_loss = -torch.mean(sherlocks_opinion)
                gen_loss.backward()
                moriarty.optimizer.step()

        if model_dir is not None:
            if show_func is not None:
                show_func(moriarty,5,epoch,model_dir=model_dir)
            if (epoch+1) % (epochs//10) == 0:
                torch.save(sherlock.state_dict(), os.path.join("models",model_dir,"sherlock_{}.sav".format(epoch+1)))
                torch.save(moriarty.state_dict(), os.path.join("models",model_dir,"moriarty_{}.sav".format(epoch+1)))

    if model_dir is not None:
        if show_func is not None:
            show_func(moriarty,10,epoch,model_dir=model_dir)
        torch.save(sherlock.state_dict(), os.path.join("models",model_dir,"sherlock_{}.sav".format(epochs)))
        torch.save(moriarty.state_dict(), os.path.join("models",model_dir,"moriarty_{}.sav".format(epochs)))
    print('Finished Training')

# weight initialization (as found in DCGAN paper)
def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)

# Ange dechu
class MLP(nn.Module):
    def __init__(self,sizes,init_method,act_funs):
        super(MLP, self).__init__()
        self.sizes = sizes
        # Build layers
        layers = []
        linears = []
        for i,(_in,_out) in enumerate(zip(sizes[:-1],sizes[1:])):
            linears.append(modules.Linear(_in,_out,init_method))
        if len(linears) < len(act_funs):
            raise Exception("Error too many activation functions!!")
        for i in zip(linears,act_funs):
            layers += [*i]
        # if there is more layers than act_funs, append the rest
        layers += linears[len(act_funs):]
        self.net = nn.Sequential(*layers)

    def set_optimizer(self, optimizer):
        self.optimizer = optimizer

    def forward(self, x):
        return self.net(x)

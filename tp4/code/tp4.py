from simple_gan import *
import torchvision.datasets as dsets
import torchvision.transforms as transforms
import torch.optim as optim
#to show imgs
from PIL import Image
from torchvision.transforms import ToPILImage
import numpy as np

to_img = ToPILImage()

def save_output(moriarty,n_img,epoch,model_dir):
    moriarty.eval()
    imgs = []
    for i in range(n_img):
        img = to_img(moriarty(Variable(moriarty.input_sampler(1,moriarty.latent_sz))).data.squeeze(0))
        img.save(os.path.join("generated",model_dir,"gen-img_epochs-{}_{}.jpg".format(epoch,i)))
        imgs.append(img)
    moriarty.train()

latent_sz = 100
img_sz = 64
bs = 128
input_sampler = lambda bs, latent_sz: torch.FloatTensor(bs, latent_sz, 1, 1).normal_(0,1)
k_steps = 4
epochs = 100

#use same criterion for both
criterion = nn.BCELoss()

transform = transforms.Compose([
    transforms.ToTensor()
])

trainset = dsets.ImageFolder(root='../data/resized_celebA/', transform=transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=bs, shuffle=True,num_workers=2)


# # Architecture

# ## Deconvolution

deconvolution = nn.Sequential(
    nn.ConvTranspose2d(latent_sz, img_sz * 8, 4, 1, 0, bias=False),
    nn.BatchNorm2d(img_sz * 8),
    nn.ReLU(True),
    # state size. (img_sz*8) x 4 x 4
    nn.ConvTranspose2d(img_sz * 8, img_sz * 4, 4, 2, 1, bias=False),
    nn.BatchNorm2d(img_sz * 4),
    nn.ReLU(True),
    # state size. (img_sz*4) x 8 x 8
    nn.ConvTranspose2d(img_sz * 4, img_sz * 2, 4, 2, 1, bias=False),
    nn.BatchNorm2d(img_sz * 2),
    nn.ReLU(True),
    # state size. (img_sz*2) x 16 x 16
    nn.ConvTranspose2d(img_sz * 2, img_sz, 4, 2, 1, bias=False),
    nn.BatchNorm2d(img_sz),
    nn.ReLU(True),
    # state size. (img_sz) x 32 x 32
    nn.ConvTranspose2d(img_sz, 3, 4, 2, 1, bias=False),
    nn.Tanh()
    # state size. (nc) x 64 x 64
    )

# Discriminator
sh_lr = 0.0002
sh_mom = 0.5
sherlock = Discriminator(img_sz)
sherlock.set_optimizer(optim.Adam(sherlock.parameters(),lr=sh_lr,betas=(sh_mom,0.999)))
_ = sherlock.apply(weights_init)

# Generator
mo_lr = 0.0002
mo_mom = 0.5
moriarty = Generator(latent_sz,img_sz,input_sampler,deconvolution)
moriarty.set_optimizer(optim.Adam(moriarty.parameters(),lr=mo_lr,betas=(mo_mom,0.999)))
_ = moriarty.apply(weights_init)

train_gan(epochs,sherlock,moriarty,k_steps,trainloader,criterion,model_dir="dcgan/deconv",show_func=save_output)


# ## Nearest-Neighbor Upsampling

class Debug(nn.Module):
    def __init__(self,title):
        super(Debug,self).__init__()
        self.title = title
    def forward(self,x):
        print(self.title,x.size())
        return x

nn_upsampling = nn.Sequential(
    # 1x1
    nn.Upsample(scale_factor=2,mode='nearest'),
    nn.Conv2d(latent_sz,img_sz*8,2,stride=2,padding=1),
    nn.BatchNorm2d(img_sz * 8),
    nn.ReLU(True),
    # 2x2
    nn.Upsample(scale_factor=2,mode='nearest'),
    nn.Conv2d(img_sz*8,img_sz*4,2,stride=2,padding=2),
    nn.BatchNorm2d(img_sz * 4),
    nn.ReLU(True),
    # 4x4
    nn.Upsample(scale_factor=2,mode='nearest'),
    nn.Conv2d(img_sz*4,img_sz*2,5,stride=1,padding=2),
    nn.BatchNorm2d(img_sz * 2),
    nn.ReLU(True),
    # 8x8
    nn.Upsample(scale_factor=2,mode='nearest'),
    nn.Conv2d(img_sz*2,img_sz,5,stride=1,padding=2),
    nn.BatchNorm2d(img_sz),
    nn.ReLU(True),
    # 16x16
    nn.Upsample(scale_factor=2,mode='nearest'),
    nn.Conv2d(img_sz,img_sz,5,stride=1,padding=2),
    nn.BatchNorm2d(img_sz),
    nn.ReLU(True),
    # 32x32
    nn.Upsample(scale_factor=2,mode='nearest'),
    nn.Conv2d(img_sz,3,2,stride=1,padding=1),
    nn.Tanh()
    # 64x64
    )

# Discriminator
sh_lr = 0.0002
sh_mom = 0.5
sherlock = Discriminator(img_sz)
sherlock.set_optimizer(optim.Adam(sherlock.parameters(),lr=sh_lr,betas=(sh_mom,0.999)))
_ = sherlock.apply(weights_init)

# Generator
mo_lr = 0.0002
mo_mom = 0.5
moriarty = Generator(latent_sz,img_sz,input_sampler,nn_upsampling)
moriarty.set_optimizer(optim.Adam(moriarty.parameters(),lr=mo_lr,betas=(mo_mom,0.999)))
_ = moriarty.apply(weights_init)

train_gan(epochs,sherlock,moriarty,k_steps,trainloader,criterion,model_dir="dcgan/nn_upsampling",show_func=save_output)


# ## Bilinear Upsampling

bilinear_upsampling = nn.Sequential(
    # 1x1
    nn.Upsample(scale_factor=2,mode='bilinear'),
    nn.Conv2d(latent_sz,img_sz*8,2,stride=2,padding=1),
    nn.BatchNorm2d(img_sz * 8),
    nn.ReLU(True),
    # 2x2
    nn.Upsample(scale_factor=2,mode='bilinear'),
    nn.Conv2d(img_sz*8,img_sz*4,2,stride=2,padding=2),
    nn.BatchNorm2d(img_sz * 4),
    nn.ReLU(True),
    # 4x4
    nn.Upsample(scale_factor=2,mode='bilinear'),
    nn.Conv2d(img_sz*4,img_sz*2,5,stride=1,padding=2),
    nn.BatchNorm2d(img_sz * 2),
    nn.ReLU(True),
    # 8x8
    nn.Upsample(scale_factor=2,mode='bilinear'),
    nn.Conv2d(img_sz*2,img_sz,5,stride=1,padding=2),
    nn.BatchNorm2d(img_sz),
    nn.ReLU(True),
    # 16x16
    nn.Upsample(scale_factor=2,mode='bilinear'),
    nn.Conv2d(img_sz,img_sz,5,stride=1,padding=2),
    nn.BatchNorm2d(img_sz),
    nn.ReLU(True),
    # 32x32
    nn.Upsample(scale_factor=2,mode='bilinear'),
    nn.Conv2d(img_sz,3,2,stride=1,padding=1),
    nn.Tanh()
    # 64x64
    )

# Discriminator
sh_lr = 0.0002
sh_mom = 0.5
sherlock = Discriminator(img_sz)
sherlock.set_optimizer(optim.Adam(sherlock.parameters(),lr=sh_lr,betas=(sh_mom,0.999)))
_ = sherlock.apply(weights_init)

# Generator
mo_lr = 0.0002
mo_mom = 0.5
moriarty = Generator(latent_sz,img_sz,input_sampler,bilinear_upsampling)
moriarty.set_optimizer(optim.Adam(moriarty.parameters(),lr=mo_lr,betas=(mo_mom,0.999)))
_ = moriarty.apply(weights_init)

train_gan(epochs,sherlock,moriarty,k_steps,trainloader,criterion,model_dir="dcgan/bilinear_upsampling",show_func=save_output)


# # WGAN

# ## Using Deconvolution

deconvolution = nn.Sequential(
    nn.ConvTranspose2d(latent_sz, img_sz * 8, 4, 1, 0, bias=False),
    nn.BatchNorm2d(img_sz * 8),
    nn.ReLU(True),
    # state size. (img_sz*8) x 4 x 4
    nn.ConvTranspose2d(img_sz * 8, img_sz * 4, 4, 2, 1, bias=False),
    nn.BatchNorm2d(img_sz * 4),
    nn.ReLU(True),
    # state size. (img_sz*4) x 8 x 8
    nn.ConvTranspose2d(img_sz * 4, img_sz * 2, 4, 2, 1, bias=False),
    nn.BatchNorm2d(img_sz * 2),
    nn.ReLU(True),
    # state size. (img_sz*2) x 16 x 16
    nn.ConvTranspose2d(img_sz * 2, img_sz, 4, 2, 1, bias=False),
    nn.BatchNorm2d(img_sz),
    nn.ReLU(True),
    # state size. (img_sz) x 32 x 32
    nn.ConvTranspose2d(img_sz, 3, 4, 2, 1, bias=False),
    nn.Tanh()
    # state size. (nc) x 64 x 64
    )

# Discriminator
sh_lr = 0.0002
sh_mom = 0.5
sherlock = Discriminator(img_sz,apply_sigmoid=False)
sherlock.set_optimizer(optim.Adam(sherlock.parameters(),lr=sh_lr,betas=(sh_mom,0.999)))
_ = sherlock.apply(weights_init)

# Generator
mo_lr = 0.0002
mo_mom = 0.5
moriarty = Generator(latent_sz,img_sz,input_sampler,deconvolution)
moriarty.set_optimizer(optim.Adam(moriarty.parameters(),lr=mo_lr,betas=(mo_mom,0.999)))
_ = moriarty.apply(weights_init)

train_wgan(epochs,sherlock,moriarty,k_steps,trainloader,criterion,model_dir="wgan/deconv",show_func=save_output)

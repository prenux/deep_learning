import os
import random

size = 64
train_path = os.path.realpath(os.curdir) + '/data/kaggle/train_{0}x{0}'.format(size)+'/'
test_path = os.path.realpath(os.curdir) + '/data/kaggle/test_{0}x{0}'.format(size)+'/'

# move 2500 images of given list
def move(l):
    for i in range(2500):
        try:
            img = random.choice(l)
            os.rename(train_path + img, test_path + img)
        except:
            pass

images = os.listdir(train_path)
cats = []
dogs = []

for img in images:
    if 'cat' in img.lower():
        cats.append(img)
    else:
        dogs.append(img)

# move 2500 random cats and 2500 random dogs to test set
move(cats)
move(dogs)





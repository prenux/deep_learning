import os

pwd = os.path.realpath(os.curdir) + '/data/kaggle'
dsets = [
        '/train_224x224/',
        '/valid_224x224/',
        '/test_224x224/'
        ]

for dset in dsets:
    cat_dir = pwd + dset + '/cats/'
    dog_dir = pwd + dset + '/dogs/'
    for dir_name in [cat_dir, dog_dir]:
        for img in os.listdir(dir_name):
            new_name = img.replace('.', '_', 1)
            old_path = dir_name + img
            new_path = dir_name + new_name
            os.rename(old_path, new_path)

        

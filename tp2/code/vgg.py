###
### from torchvision.models source code 
### THANKS GUYS!!! GOD DAMN IT THIS IS GREATTT!!!
###
### http://pytorch.org/docs/0.3.0/_modules/torchvision/models/vgg.html
###

import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.utils.model_zoo as model_zoo
import torchvision
import torchvision.datasets as dsets
import torchvision.transforms as transforms
import math


# Modify number of classes to 2 because we only have cats and dogs
# apply sigmoid to output of last linear layer
class VGG(nn.Module):
    def __init__(self, features, num_classes=2):
        super(VGG, self).__init__()
        self.features = features
        self.classifier = nn.Sequential(
            nn.Linear(2048, 4096),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(4096, 4096),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(4096, num_classes)
        )
        self._initialize_weights()

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        return x
    
    # initialization scheme, wont be used when transfer learning is applied (aka pretrained=True)
    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                m.weight.data.normal_(0, 0.01)
                m.bias.data.zero_()


def make_layers(cfg, batch_norm=False):
    layers = []
    in_channels = 3
    for i,v in enumerate(cfg):
        ### Max pooling layer
        # kernel 2x2
        # stride 2
        # padding 0
        if v == 'M':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        else:
            ### Conv layer
            # kernel 3x3
            # stride 1
            # padding 1 
            # (in - 3 + 2) + 1 = in --> OK preserve dimensionality
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            if batch_norm :
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    return nn.Sequential(*layers)


# we are using VGG16 with batch norm, which corresponds to network "D" in
# Very Deep Convolutional Networks for Large-Scale Image Recognition
# K. Simonyan, A. Zisserman
def vgg16_bn(pretrained=False, **kwargs):
    cfg = [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M', 512, 512, 512, 'M']
    model = VGG(make_layers(cfg, batch_norm=True), **kwargs)
    if pretrained:
        model.load_state_dict(get_pretrained_weights())
    return model

def vgg11_bn(pretrained=False, **kwargs):
    cfg = [64, 'M', 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M']
    model = VGG(make_layers(cfg, batch_norm=True), **kwargs)
    if pretrained:
        model.load_state_dict(model_zoo.load_url(model_urls['vgg11_bn']))
    return model

# need to modify weights and biases from preloaded values for last layer
def get_pretrained_weights():
    model_url = 'https://download.pytorch.org/models/vgg16_bn-6c64b313.pth'
    state_dict = model_zoo.load_url(model_url)
    state_dict['classifier.6.weight'] = nn.init.xavier_normal(torch.FloatTensor(2,4096))
    state_dict['classifier.6.bias'] = torch.zeros(2)
    return state_dict

# return training or validation error
def get_error(model, loader):
    err = 0
    for ex in loader:
        inputs,labels = ex
        inputs = Variable(inputs)
        labels = Variable(labels)
        err += 100 - float(((torch.max(model(inputs),1)[1]==labels).float().mean())*100.0)
    return err / len(loader)


###
### Debugging
###
if __name__ == "__main__":
    # Parameters, uses early_stopping, transfer_learning & nesterov
    lr = 0.02
    bs = 10
    wd = 5 * 10**(-4)
    mom = 0.9
    epochs = 100

    # Load 224 x 224 images and normalize them (as suggested on PyTorch doc) 
    # Training set size: 15569
    # Validation set size: 5000
    # Test set size: 4432
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])

    trainset = dsets.ImageFolder(root='data/kaggle/train_224x224',transform=transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=bs,shuffle=True, num_workers=2)

    validset = dsets.ImageFolder(root='data/kaggle/valid_224x224',transform=transform)
    validloader = torch.utils.data.DataLoader(validset, batch_size=bs,shuffle=True, num_workers=2)

    testset = dsets.ImageFolder(root='data/kaggle/test_224x224',transform=transform)
    testloader = torch.utils.data.DataLoader(testset, batch_size=bs,shuffle=True, num_workers=2)

    # Load model
    bengio = vgg16_bn(pretrained=True)
    optimizer = torch.optim.SGD(bengio.parameters(),lr=lr, momentum=mom, weight_decay=wd, nesterov=True)
    criterion = nn.CrossEntropyLoss() # or crossentropy with 2 classes, need labels as 0 and 1

    # global params
    train_errors = []
    valid_errors = []
    best_valid_error = 1000
    count = 0
    max_epoch_no_improvement = 5

    # train model, print running_loss at end of each epoch
    for epoch in range(epochs):
        running_loss = 0.0
        for data in trainloader:
            inputs, labels = data
            inputs, labels = Variable(inputs), Variable(labels)
            optimizer.zero_grad()
            outputs = bengio(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            running_loss += loss.data[0]
            print(loss.data[0])
        # update best validation error
        valid_error = get_error(bengio, validloader)
        if valid_error < best_valid_error:
            best_valid_error = valid_error
            count = 0
        else:
            count += 1
            if count >= max_epoch_no_improvement:
                break
        valid_errors.append(valid_error)
        train_errors.append(get_error(bengio,trainloader))
        mean_loss = running_loss/len(trainloader)
        print('[%d] Mean Train loss: %.3f' % (epoch, mean_loss))
    print('Finished Training')


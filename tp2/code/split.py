import os

size = 64
pwd = os.path.realpath(os.curdir) + '/data/kaggle'
dsets = [
        '/train_{0}x{0}'.format(size)+'/',
        '/valid_{0}x{0}'.format(size)+'/',
        '/test_{0}x{0}'.format(size)+'/'
        ]

for dset in dsets:
    root = pwd + dset
    cat_dir = root + '/cats/'
    dog_dir = root + '/dogs/'
    for img_name in os.listdir(root):
        cur_path = root + img_name
        if not(os.path.isdir(cur_path)):
            if 'cat' in img_name.lower():
                os.rename(cur_path, cat_dir + img_name)
            else:
                os.rename(cur_path, dog_dir + img_name)

        

# Greatly inspired by http://pytorch.org/docs/master/notes/extending.html
import math
from torch import nn
import torch
import torch.nn.functional as F

class Linear(nn.Module):
    def __init__(self, input_features, output_features, weight_init_method,
            bias=True, bias_init_method=torch.zeros_like):
        super(Linear, self).__init__()
        self.input_features = input_features
        self.output_features = output_features
        self.weight = nn.Parameter(weight_init_method(torch.Tensor(output_features, input_features)))
        if bias:
            self.bias = nn.Parameter(bias_init_method(torch.Tensor(output_features)))
        else:
            self.register_parameter('bias', None)

    def forward(self, input):
        return F.linear(input, self.weight, self.bias)

def l2_pen(x):
    return math.sqrt(torch.pow(x,2).sum())

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torchvision
import torchvision.transforms as transforms
from sklearn.model_selection import train_test_split
from torch.utils.data.sampler import SubsetRandomSampler
import math

import modules

class MLP(nn.Module):
    def __init__(self, sizes, init_method,dropout_last=False,apply_smax=True):
        super(MLP,self).__init__()
        self.sizes = sizes
        self.dropout_last = dropout_last
        self.apply_smax = apply_smax
        # Build layers
        layers = []
        for i,(_in,_out) in enumerate(zip(sizes[:-1],sizes[1:])):
            if dropout_last and i == len(sizes)-1:
                layers.append(nn.Dropout(inplace=True))
            layers.append(modules.Linear(_in,_out,init_method))
            if i < len(sizes)-2:
                layers.append(nn.ReLU())
        self.net = nn.Sequential(*layers)

    def forward(self,x):
        res = self.net(x)
        if self.apply_smax:
            return F.softmax(res,dim=1)
        else:
            return res

    def set_apply_smax(self,value):
        self.apply_smax = value

    def get_weights(self):
        res = []
        for name,data in self.named_parameters():
            if "weight" in name:
                res.append(data.view(-1))
        return torch.cat(res)

    def crazy_train(self,trainloader,epochs,criterion,optimizer,quiet=False,
            norm_func=modules.l2_pen,weight_decay=0):
        train_losses = []
        norms = []
        self.set_apply_smax(True)
        for epoch in range(epochs):  # loop over the dataset multiple times
            running_loss = 0.0
            for i, data in enumerate(trainloader,0):
                # get the inputs
                inputs, labels = data
                # wrap them in Variable
                inputs, labels = Variable(inputs), Variable(labels)
                # zero the parameter gradients
                optimizer.zero_grad()
                # forward + backward + optimize
                outputs = self(inputs)
                # norm
                norm = norm_func(self.get_weights())
                # reg
                reg = 0.5*weight_decay/trainloader.batch_size*norm
                loss = criterion(outputs, labels) + reg
                loss.backward()
                optimizer.step()
                running_loss += loss.data[0]
                norms.append(norm)
            mean_loss = running_loss/len(trainloader)
            if not quiet:
                print('[%d] Mean Train loss: %.3f' %
                        (epoch, mean_loss))
            running_loss = 0.0
            train_losses.append(mean_loss)
        if not quiet:
            print('Finished Training')
        return train_losses,norms

def load_MNIST(train_bs,test_bs):
    # Datasets
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Lambda(lambda x: x.view(784))])
    trainset = torchvision.datasets.MNIST(root="./data", train=True,download=True, transform=transform)
    testset = torchvision.datasets.MNIST(root="./data", train=False,download=True, transform=transform)
    # Loaders
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=train_bs,shuffle=True, num_workers=2)
    testloader = torch.utils.data.DataLoader(testset, batch_size=test_bs,shuffle=True, num_workers=2)
    return trainloader,testloader

def load_MNIST_with_valid(batch_size,train_ratio):
    # Datasets
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Lambda(lambda x: x.view(784))])
    trainset = torchvision.datasets.MNIST(root="./data", train=True,download=True, transform=transform)
    testset = torchvision.datasets.MNIST(root="./data", train=False,download=True, transform=transform)
    # Loaders
    train_ind,valid_ind = train_test_split(np.arange(len(trainset)),train_size=train_ratio)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size,
            num_workers=2, sampler=SubsetRandomSampler(train_ind))

    validloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size,
            num_workers=2, sampler=SubsetRandomSampler(valid_ind))

    testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size,
            shuffle=True, num_workers=2)
    return trainloader,validloader,testloader

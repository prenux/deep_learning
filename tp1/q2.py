import numpy as np

# feedforward for number two
# coord is np.array (1x2)
def ff(coord):
    W = np.array([[5,-6,-4], [4,4,-4]])
    b = np.array([20,24,16])
    U = np.array([1,1,1])
    c = -3
    a = heavy(np.dot(coord, W) + b)
    return 1 if (np.dot(a,U) + c) >= 0 else 0

# heavyside function
def heavy(m):
    m[m == 0] = 1
    return m.clip(0,1)


import sys
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import optim
from torch.autograd import Variable
import torchvision
import torchvision.transforms as transforms
from sklearn.model_selection import train_test_split
from torch.utils.data.sampler import SubsetRandomSampler
import math

import modules

h0 = 784
h1 = 425     #TODO Update value
h2 = 425     #TODO Update value
h3 = 10

sizes = [h0,h1,h2,h3]

class MLP(nn.Module):
    def __init__(self, sizes, init_method):
        super(MLP,self).__init__()
        self.sizes = sizes
        # Build layers
        layers = []
        for i,(_in,_out) in enumerate(zip(sizes[:-1],sizes[1:])):
            layers.append(modules.Linear(_in,_out,init_method))
            if i < len(sizes)-2:
                layers.append(nn.ReLU())
            else:
                layers.append(nn.Softmax(dim=1))
        self.net = nn.Sequential(*layers)

    def forward(self,x):
        return self.net(x)

    def train(self,trainloader,epochs,criterion,optimizer, quiet=False,
            ret_test_losses=False, testloader=None,trainset_ratio=1,verbose=False):
        # Trainset ratio is used for splitting trainset in true trainset and validation set
        # if we want to return the losses
        train_losses = []
        verbose_losses = []
        if ret_test_losses and testloader is not None:
            print("Will return test losses too")
            test_losses = []
        trainset_size = int(trainset_ratio*len(trainloader))
        for epoch in range(epochs):  # loop over the dataset multiple times
            running_loss = 0.0
            for i, data in enumerate(trainloader,0):
                # the rest is validtion set so stop
                if i > trainset_size:
                    break
                # get the inputs
                inputs, labels = data
                # wrap them in Variable
                inputs, labels = Variable(inputs), Variable(labels)
                # zero the parameter gradients
                optimizer.zero_grad()
                # forward + backward + optimize
                outputs = self(inputs)
                loss = criterion(outputs, labels)
                loss.backward()
                optimizer.step()
                running_loss += loss.data[0]
                if verbose:
                    verbose_losses.append(loss.data[0])
            mean_loss = running_loss / trainset_size
            if not quiet:
                print('[%d] Mean Train loss: %.3f' %
                        (epoch, mean_loss))
            running_loss = 0.0
            train_losses.append(mean_loss)
            if ret_test_losses and testloader is not None:
                test_loss = self.test(testloader,criterion)
                if not quiet:
                    print('[%d] Test loss: %.3f' %
                            (epoch, test_loss))
                test_losses.append(test_loss)
        if not quiet:
            print('Finished Training')
        if ret_test_losses and testloader is not None:
            return train_losses,test_losses
        if verbose:
            return verbose_losses
        return train_losses

    def test(self,testloader,criterion):
        running_loss = 0
        for i,data in enumerate(testloader,0):
            # get the inputs
            inputs, labels = data
            # wrap them in Variable
            inputs, labels = Variable(inputs), Variable(labels)
            # forward
            outputs = self(inputs)
            loss = criterion(outputs, labels)
            running_loss += loss.data[0]
        mean_loss = running_loss / len(testloader)
        return mean_loss

def load_MNIST(batch_size):
    # Datasets
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Lambda(lambda x: x.view(784))])
    trainset = torchvision.datasets.MNIST(root="./data", train=True,download=True, transform=transform)
    testset = torchvision.datasets.MNIST(root="./data", train=False,download=True, transform=transform)
    # Loaders
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size,shuffle=True, num_workers=2)
    testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size,
            shuffle=True, num_workers=2)
    return trainloader,testloader

def load_MNIST_with_valid(batch_size,train_ratio):
    # Datasets
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Lambda(lambda x: x.view(784))])
    trainset = torchvision.datasets.MNIST(root="./data", train=True,download=True, transform=transform)
    testset = torchvision.datasets.MNIST(root="./data", train=False,download=True, transform=transform)
    # Loaders
    train_ind,valid_ind = train_test_split(np.arange(len(trainset)),train_size=train_ratio)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size,
            num_workers=2, sampler=SubsetRandomSampler(train_ind))

    validloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size,
            num_workers=2, sampler=SubsetRandomSampler(valid_ind))

    testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size,
            shuffle=True, num_workers=2)
    return trainloader,validloader,testloader


if __name__=="__main__":
    trainloader,testloader = load_MNIST()
    gbrassard = MLP(sizes)
    # Some testing
    print(gbrassard.net)
    out = gbrassard(Variable(torch.rand(1,784)))
    print("Sum:",out.sum())
    print(out)
    # used for loading already existing model (i.e. sys.argv[1] is path to state_dict)
    if len(sys.argv) > 1:
        try:
            gbrassard.load_state_dict(torch.load(sys.argv[1]))
        except FileNotFoundError:
            print("Cant find state dict file")
            exit(-1)
    # Training params
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(gbrassard.parameters(), lr=0.001, momentum=0.9)
    epochs = 10
    # Train it!
    gbrassard.train(trainloader,epochs,criterion,optimizer)
    # Save model state_dict
    torch.save(net.state_dict(), "nn.sav")
